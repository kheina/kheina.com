function indexOnLoad()
{
	let maxrating = parseInt(getCookie('maxrating'));
	switch(maxrating)
	{
		case 1:
			maxrating = 'Mature';
			break;
		case 2:
			maxrating = 'Explicit';
			break;
		default:
			maxrating = 'General';
			break;
	}

	let searchParams = new URLSearchParams(window.location.search.toLowerCase());

	if (searchParams.has('url'))
	{
		let form = document.querySelector('form.centerx');
		form.style.left = '1000%';
		let cover = document.querySelector('div.loadingcover');
		cover.style.opacity = 0;

		sendApiCall('url', searchParams.get('url'));
	}
	else
	{ document.getElementById(maxrating).checked = true; }
}

function search()
{
	let form = document.querySelector('form.centerx');
	let formurl = form.querySelector('input#url');
	let formfile = form.querySelector('input#file');

	if (formfile.value || formurl.value.startsWith('http:') || formurl.value.startsWith('https:'))
	{
		form.style.left = '1000%';
		let cover = document.querySelector('div.loadingcover');
		cover.style.opacity = 0;

		if (formfile.value)
		{ sendApiCall('file', formfile.files[0]); }
		else if (formurl.value)
		{ sendApiCall('url', formurl.value); }
	}
}

function sendApiCall(field, data)
{
	let div = document.createElement('div');
	div.innerHTML = '<span id="uploadtotal"></span><p style="right: 4px;">Upload</p>';
	let uploadprogress = document.getElementById('uploadprogress');
	uploadprogress.appendChild(div);
	uploadprogress.style.display = 'block';

	let formdata = new FormData();
	formdata.append(field, data);

	let ajax = new XMLHttpRequest();

	ajax.upload.addEventListener('progress', (event) => progressHandler(event, div), false);
	ajax.addEventListener('load', resultsOnLoad, false);
	ajax.addEventListener('error', errorHandler, false);
	// ajax.addEventListener('abort', abortHandler, false);

	ajax.open('POST', 'https://api.kheina.com/v1/search'); // this needs to be changed for local search testing

	ajax.send(formdata);
}

function progressHandler(event, div)
{
	let percent = (event.loaded / event.total) * 100;
	let progressbar = div.childNodes[0];

	progressbar.style.width = percent.toString() + '%';
	progressbar.innerHTML = percent.toFixed(2) + '%';
	if (percent >= 100)
	{
		let text = div.childNodes[1];
		text.innerHTML = 'processing...';
		text.style = '';
		text.style.width = '100%';
		text.style.textAlign = 'center';
	}
}

function onUnload()
{
	let form = document.querySelector('form.centerx');
	form.style.left = '';
	let cover = document.querySelector('div.loadingcover');
	cover.style.opacity = 1;
}

function resultsOnLoad(event)
{
	try
	{
		let response = JSON.parse(event.target.responseText);
		let templates = HarvestTemplates(document.getElementById('templates'));

		if (response.error)
		{
			error = response.error;
			if (response.stacktrace)
			{ error += '\n	stacktrace:\n' + response.stacktrace.join('').replace(/\s*$/,''); }
			throw error;
		}

		let searchParams = new URLSearchParams(window.location.search.toLowerCase());

		let maxrating = undefined;
		if (searchParams.has('maxrating'))
		{ maxrating = searchParams.get('maxrating'); }
		else
		{ maxrating = getCookie('maxrating'); }
		switch(maxrating.toLowerCase())
		{
			case '1':
				maxrating = 1;
				break;
			case 'mature':
				maxrating = 1;
				break;
			case '2':
				maxrating = 2;
				break;
			case 'explicit':
				maxrating = 2;
				break;
			default:
				maxrating = 0;
				break;
		}

		let matchcolor = 0x00ff00; // green
		let falsecolor = 0xff0000; // red

		results = response.results;

		// results.sort((a, b) => b[0].similarity - a[0].similarity );
		// results = FindSameImages(results);
		// results = SortBySize(results);

		let constructedResults = templates[0];

		let topresult = constructedResults.querySelector('div.top data');


		let topele = InsertTopResult(results[0], maxrating, matchcolor, falsecolor, templates[1]);
		let more = document.getElementById('more');

		topresult.parentNode.replaceChild(topele, topresult);

		more.parentNode.replaceChild(InsertMoreResults(results, maxrating, matchcolor, falsecolor, templates[2]), more);

		let timeele = InsertSearchTime(response.stats, response.elapsedtime);

		document.getElementsByClassName('searchstats')[0].appendChild(timeele);
		setStats(response.stats, document.getElementById('progress'));

		document.getElementById('feature').innerHTML = constructedResults.innerHTML;

		// ideally this would be handled within InsertTopResult, but that's not possible without doing the above replace
		document.querySelector('.top .source').style.height = document.querySelector('.top .source div.links').clientHeight + 16;
	}
	catch (error)
	{
		// at the very least, log the error to console
		console.error(error);
		insertError(error);
	}
}

function errorHandler(event)
{
	// at the very least, log the error to console
	console.error(event);

	let response = JSON.parse(event.target.responseText);

	let error = undefined;

	if (response.error)
	{ error = response.error; }
	else
	{ error = "an unknown error occurred during the api call."; }

	if (response.stacktrace)
	{ error += '\n	stacktrace:\n' + response.stacktrace.join('').replace(/\s*$/,''); }

	insertError(error);
}

function insertError(error)
{
	// now try to display the error in a user-friendly way
	let templates = HarvestTemplates(document.getElementById('templates'));
	let constructedError = templates[templates.length - 1];

	let errorData = constructedError.querySelector('data#errormessage');
	let errorMessage = document.createElement('pre');

	errorMessage.className = 'message';
	errorMessage.innerHTML = error;
	errorData.appendChild(errorMessage);
	errorData.style.display = 'block';

	document.getElementById('feature').innerHTML = constructedError.innerHTML;
}
