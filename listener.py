from common.caching import SimpleCache
from crc16 import crc16xmodem as cksum
from common.crawling import tagSplit
from common import logging
import requests
import json
import time
import sys
import os


logger = logging.getLogger('notifier', disable=['pika'])


class QuitParsing(Exception) :
	pass


def deleteUser(user) :
	userPath = f'notifier/data/{user}.json'
	if os.path.isfile(userPath) :
		try :
			os.remove(userPath)
		except :
			logger.exception('unable to delete user profile.')
			return False
		return True
	return False


class Listener :

	# commands that don't need to run any logic
	BasicCommands = {
		'/start': 'This bot sends you alerts any time a new image is indexed by kheina.com. you can control what you get alerted for via tags.',
		'/help': '\n'.join([
			'<b>general info:</b>',
			'    tags: you will receive an alert any time an image with your given tags is indexed. ex: <code>/add snow_leopard</code>',
			'    tag groups: you can submit groups of tags with will send you an alert only when all tags in a given group are matched. ex: <code>/add snow, leopard</code>',
			'    negated tags: you can also match on the absense of a tag. you can do this by preceding the tag with a "!" character. ex: <code>/add snow_leopard, !explicit</code>',
			'    the blacklist: images matching the tags in your blacklist will never be sent to you. ex: <code>/addbl gore</code>',
			'',
			'<b>helpful commands:</b>',
			'    /commands: display all available commands and what they do.',
			'    /advanced: display a few different advanced features this bot is capable of.',
		]),
		'/commands': '\n    '.join([
			'<b>commands:</b>',
			'/commands: shows this menu.',
			'/help: explains how the tagging system this bot offers works.',
			'/advanced: display a few different advanced features this bot is capable of.',
			'/whitelist: displays the tags you are currently alerted by.',
			"/blacklist: displays tags that you won't receive alerts for.",
			'/add: adds a single tag or tag group to your whitelist.',
			'/remove: removes a single tag or tag group from your whitelist.',
			'/addbl: adds a single tag or tag group to your blacklist.',
			'/removebl: removes a single tag or tag group from your blacklist.',
		]),
		'/advanced': '\n    '.join([
			'<b>slash modifiers:</b>',
			'you can add a "/" and a character at the end of your tag to make it exclusive to a specific function.',
			'_/a: only match the artist (even if the artist exists as a tag). ex: <code>/add glopossum/a</code>',
			'_/r: only match the rating flag. ex: <code>/addbl explicit/r</code>',
		]),
	}


	def __init__(self, looptime=1, ttl=float('inf')) :
		self.looptime = looptime
		self.TTL = time.time() + ttl

		with open('credentials/notifier.json') as credentials :
			credentials = json.load(credentials)
			self._telegram_access_token = credentials['telegram_access_token']
			self._telegram_bot_id = credentials['telegram_bot_id']

		# commands that actually require logic to be performed
		self.commands = {
			'/whitelist': self.sendWhitelist,
			'/blacklist': self.sendBlacklist,
			'/add': self.addTag,
			'/remove': self.removeTag,
			'/addbl': self.addBlacklistTag,
			'/removebl': self.removeBlacklistTag,
			'/rb': self.rb,
			'/rw': self.rw,
			'/stop': self.stop,
			'/pause': self.pause,
		}


	def sendMessage(self, user, message) :
		# mode = MarkdownV2 or HTML
		request = f'https://api.telegram.org/bot{self._telegram_access_token}/sendMessage?chat_id={user}&parse_mode=HTML&text='
		url = request + message
		info = 'failed to send message to telegram.'
		for _ in range(5) :
			try :
				info = json.loads(requests.get(url).text)
				if not info['ok'] :
					break
				return True
			except :
				pass
		logger.error({
			'info': info,
			'request': request,
			'message': message,
			'user': user,
		})
		return False


	def stop(self, user, text) :
		if text.lower() == 'delete' :
			if deleteUser(user) :
				self.sendMessage(user, 'done, your profile has been deleted.')
			else :
				self.sendMessage(user, "hmm, it looks like something weird happened and I wasn't able to delete your profile.\nplease message @darius assistance.")
		elif text :
			self.sendMessage(user, "I couldn't understand your request, run <code>/stop delete</code> to delete your profile.")
		else :
			self.sendMessage(user, 'WARNING!\nthis will delete your whole profile!\n\nrun <code>/stop delete</code> if you are sure.')


	def pause(self, user, text) :
		self.sendMessage(user, 'sorry, this does nothing yet (coming soonish, maybe)')


	def filter(self, user) :
		if user in Listener.loadAllowedUsers() :
			return True

		logger.warning({
			'message': 'unauthorized user attempted to utilize bot.',
			'user': user,
			'allowed users': list(Listener.loadAllowedUsers()),
		})
		self.sendMessage(user, "sorry, this bot is currently only available for kheina.com patrons.\n\nif you are a patron, please message @darius to be added to the list of allowed users.\n\nif you are interested in becoming a patron, visit <a href=\"https://www.patreon.com/kheina\">patreon.com/kheina</a>")

		return False


	def loadUserData(self, user, default=None) :
		try :
			with open(f'notifier/data/{user}.json') as data :
				return json.load(data)
		except FileNotFoundError :
			if default is not None :
				return default
			self.sendMessage(user, "it doesn't look like you have a profile yet, try adding some tags.")
		raise QuitParsing('no profile found.')


	def writeUserData(self, user, data) :
		with open(f'notifier/data/{user}.json', 'w') as userfile :
			return json.dump(data, userfile)


	def tagHash(self, tag) :
		return str(cksum(str(tag).encode())).rjust(5, '0')


	def rw(self, user, text) :
		userData = self.loadUserData(user)
		try :
			index, tags = next(filter(lambda x : self.tagHash(x[1]) == text, enumerate(userData['whitelist'])))
		except :
			logger.exception({
				'message': 'tag not found.',
				'text': text,
				'user': user,
				'whitelist': userData.get('whitelist'),
			})
			self.sendMessage(user, "sorry, I couldn't find that tag in your whitelist")
		else :
			del userData['whitelist'][index]
			self.writeUserData(user, userData)
			self.sendMessage(user, 'okay, <i>' + ', '.join(tags) + '</i> has been removed from your whitelist')


	def rb(self, user, text) :
		userData = self.loadUserData(user)
		try :
			index, tags = next(filter(lambda x : self.tagHash(x[1]) == text, enumerate(userData['blacklist'])))
		except :
			logger.exception({
				'message': 'tag not found.',
				'text': text,
				'user': user,
				'blacklist': userData.get('blacklist'),
			})
			self.sendMessage(user, "sorry, I couldn't find that tag in your blacklist")
		else :
			del userData['blacklist'][index]
			self.writeUserData(user, userData)
			self.sendMessage(user, 'okay, <i>' + ', '.join(tags) + '</i> has been removed from your blacklist')


	def sendWhitelist(self, user, text) :
		userData = self.loadUserData(user)
		if 'whitelist' in userData :
			self.sendMessage(user, 'here are the tags currently in your whitelist:\n    ' + '\n    '.join([
				f'/rw{self.tagHash(tags)} ' + ', '.join(tags)
				for tags in userData['whitelist']
			]))
		else :
			self.sendMessage(user, "it doesn't look like you have a whitelist yet, try adding some tags.")


	def sendBlacklist(self, user, text) :
		userData = self.loadUserData(user)
		if 'blacklist' in userData :
			self.sendMessage(user, 'here are the tags currently in your blacklist:\n    ' + '\n    '.join([
				f'/rb{self.tagHash(tags)} ' + ', '.join(tags)
				for tags in userData['blacklist']
			]))
		else :
			self.sendMessage(user, "it doesn't look like you have a blacklist yet, try adding some tags.")


	def addTag(self, user, tag) :
		tags = tuple(tagSplit(tag))
		if not tags :
			self.sendMessage(user, "I can't add empty tags to your whitelist")
			return
		userData = self.loadUserData(user, { })
		userData['whitelist'] = set(map(tuple, userData.get('whitelist', [])))
		userData['whitelist'].add(tags)
		userData['whitelist'] = list(userData['whitelist'])
		self.writeUserData(user, userData)
		self.sendMessage(user, 'okay, <i>' + ', '.join(tags) + '</i> has been added to your whitelist')


	def removeTag(self, user, tag) :
		userData = self.loadUserData(user)
		tags = list(tagSplit(tag))
		try : userData['whitelist'].remove(tags)
		except :
			self.sendMessage(user, "sorry, it doesn't look like <i>" + ', '.join(tags) + '</i> is in your whitelist')
		else :
			self.writeUserData(user, userData)
			self.sendMessage(user, 'okay, <i>' + ', '.join(tags) + '</i> has been removed from your whitelist')


	def addBlacklistTag(self, user, tag) :
		tags = tuple(tagSplit(tag))
		if not tags :
			self.sendMessage(user, "I can't add empty tags to your blacklist")
			return
		userData = self.loadUserData(user, { })
		userData['blacklist'] = set(map(tuple, userData.get('blacklist', [])))
		userData['blacklist'].add(tags)
		userData['blacklist'] = list(userData['blacklist'])
		self.writeUserData(user, userData)
		self.sendMessage(user, 'okay, <i>' + ', '.join(tags) + '</i> has been added to your blacklist')


	def removeBlacklistTag(self, user, tag) :
		userData = self.loadUserData(user)
		tags = list(tagSplit(tag))
		try : userData['blacklist'].remove(tags)
		except :
			self.sendMessage(user, "sorry, it doesn't look like <i>" + ', '.join(tags) + '</i> is in your blacklist')
		else :
			self.writeUserData(user, userData)
			self.sendMessage(user, 'okay, <i>' + ', '.join(tags) + '</i> has been removed from your blacklist')


	def parseMessage(self, message) :
		user = message['from']['id']
		chat = message['chat']['id']
		if user != chat or not self.filter(user) :
			return True

		if not 'entities' in message :
			return self.sendMessage(user, 'sorry, I only understand bot commands right now.')

		entity = next(filter(lambda x : x['type'] == 'bot_command', message['entities']))
		if not entity :
			return self.sendMessage(user, 'sorry, I only understand bot commands right now.')

		end = entity['offset'] + entity['length']
		command = message['text'][entity['offset']:end]

		if command in self.BasicCommands :
			return self.sendMessage(user, self.BasicCommands[command])

		text = message['text'][end:].strip()

		if command in self.commands :
			return self.commands[command](user, text)

		if command.startswith('/r') :
			return self.commands[command[:3]](user, command[3:])

		self.sendMessage(user, "sorry, I didn't understand that command. to see a list of my commands, try /help")


	def run(self) :

		count = 0
		for update in self.recv() :
			try :
				self.parseMessage(update['message'])

			except QuitParsing :
				pass

			except :
				logger.exception({
					'message': 'failed to parse message.',
					'message': update,
				})

			finally :
				count += 1


	def recv(self) :
		# just let it fail if it's not json serialized
		request = f'https://api.telegram.org/bot{self._telegram_access_token}/getUpdates?offset='
		mostrecent = 0
		while True :
			try :
				updates = json.loads(requests.get(request + str(mostrecent)).text)
				if updates['ok'] and updates['result'] :
					mostrecent = updates['result'][-1]['update_id'] + 1
					yield from updates['result']
			except :
				logger.exception('failed to read updates from telegram.')

			if self.looptime + time.time() > self.TTL :
				return
			time.sleep(self.looptime)


	@SimpleCache(30)
	def loadAllowedUsers() :
		return set(json.load(open('notifier/allowed_users.json')))


if __name__ == '__main__' :
	from ast import literal_eval
	kwargs = { k: literal_eval(v) for k, v in (arg.split('=') for arg in sys.argv[1:]) }
	try :
		listener = Listener(**kwargs)
		listener.run()
	finally :
		pass
