import hashlib
import secrets
import time

class Token() :
	def __init__(self, hash, salt) :
		self._hash = hash
		self._salt = salt
	
	def hash(self) :
		return self._hash

	def salt(self) :
		return self._salt

class Key() :
	class InvalidKey(Exception) :
		pass

	def __init__(self, TTL=86400) :
		self._login = secrets.token_hex(128)
		self._refresh = secrets.token_hex(64)
		self._expires = time.time() + TTL
	
	def verifyLogin(self, loginKey, refreshKey=None) :
		if loginKey == self._login and time.time() < self._expires :
			return None
		if refreshKey :
			return self.verifyRefresh(loginKey, refreshKey)
		raise InvalidKey('the key does not match, or it has expired')

	def verifyRefresh(self, loginKey, refreshKey) :
		if loginKey == self._login and refreshKey == self._refresh :
			return generateKey()
		raise InvalidKey('the keys do not match')


def hash(password, salt=None) :
	hasher = hashlib.sha3_512()
	if not salt : salt = secrets.token_bytes(64)
	hasher.update(password.encode())
	hasher.update(salt)
	return Token(hasher.digest(), salt)

def generateKey(TTL=86400) :
	return Key(TTL=TTL)
