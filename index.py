
async def indexGet(req, log) :
	if 'url' in req.query_params :
		log['url'] = req.query_params['url']
		return parsesource.loadimagedata(req.query_params['url'])
	else :
		return None

async def indexPost(req, log) :
	formdata = await req.body()
	formdata = parsesource.parseformdata(formdata)
	if type(formdata) is dict :
		if 'file' in formdata :
			return formdata['file']['contenttype']
		elif 'url' in formdata :
			log['url'] = formdata['url']
			return parsesource.loadimagedata(formdata['url'])
		else : raise HTTPError.BadRequest('No form data provided.')

indexSwitch = {
	'GET': indexGet,
	'POST': indexPost,
	# don't need to define a default, because it's guaranteed to be in here
	# and it would get caught by the try anyway
}

METHODS = list(indexSwitch.keys())

async def handle(req, res, log) :
	severity = 'INFO'
		stats = await handle_json('stats')
		imagedata = await indexSwitch[req.method](req, log)
		if imagedata :
			imagebytescount = str(len(imagedata))
			log['bytes'] = imagebytescount
			initialtime = time.time()
			results, iqdbdata = databaser.queryiqdb(imagebytescount.encode(), imagedata)
			elapsedtime = time.time() - initialtime
			log['elapsedTime'] = elapsedtime
			if results :
				log['similarity'] = iqdbdata[0]['similarity']
				res.html = await handle_text('results.html') + '<data id="results">' + json.dumps(results,escape_forward_slashes=False) + '</data><data id="iqdbdata">' + json.dumps(iqdbdata) + '</data><data id="elapsedtime">' + str(elapsedtime) + '</data><data id="stats">' + stats + '</data>'
			else :
				severity = 'ERROR'
				log['error'] = 'iqdb error:\n' + str(iqdbdata[1]) + '\nmessage:\n  ' + str(iqdbdata[0])
				res.html = await handle_text('error.html') + '<data id="error">' + log['error'] + '</data><data id="stats">' + stats + '</data>'
		else :
			res.html = await handle_text('index.html') + '<data id="stats">' + stats + '</data>'
		logger.agent.log_struct(log, severity=severity)
	except parsesource.BadRequest as e : await status400badrequest(res, exception=e)
	except parsesource.UnsupportedMedia as e : await status415unsupportedmedia(res, exception=e)
	except Exception as e : await status500internalserver(res, e)
