from pycrawl.crawler import WebsiteOffline, InvalidResponseType, InvalidSubmission, NoSubmission, first
from pycrawl.common.HTTPError import BadOrMalformedResponse
from common.caching import SimpleCache
from baseCrawler import BaseCrawler
from pycrawl.common import isint
import sys

class FurAffinityCrawler(BaseCrawler) :
	submissionTypes = { 'story', 'music' }

	def __init__(self, *args, **kwargs) :
		BaseCrawler.__init__(self, *args, key='furaffinity', domain='.furaffinity.net', **kwargs)
		self.thumbnailIndex = 2


	def formatUrl(self, url) :
		return f'https://www.furaffinity.net/view/{url}'


	def parse(self, document) :
		# check that the website isn't down and etc etc
		if first(document.xpath('//body//div[@class="attribution"]/a/text()', **self.xpathargs)) == 'DDoS protection by Cloudflare' :
			raise WebsiteOffline('furaffinity is currently behind cloudflare.')

		if first(document.xpath('//body/@id', **self.xpathargs)) == 'pageid-matureimage-error' :
			raise BadOrMalformedResponse('furaffinity login error.')

		elif document.xpath('//head/title[contains(text(), "System Error")]', **self.xpathargs) and document.xpath('//body/section/div[@class="section-body" and contains(text(), "The submission you are trying to find is not in our database")]', **self.xpathargs) :
			raise NoSubmission('url does not have a submission.')

		elif document.xpath('//img[@src="/fa_offline.jpg"]', **self.xpathargs) :
			raise WebsiteOffline('furaffinity is currently offline.')

		# now we can actually crawl

		imageurl = first(document.xpath('//img[@id="submissionImg"]/@src', **self.xpathargs))
		if not imageurl :
			raise InvalidResponseType('submission does not contain an image.')

		filetype = document.xpath('//div[@class="submission-content"]//center[contains(@class, "p20")]/div[contains(strong/text(), "File type")]', **self.xpathargs)
		if filetype :
			raise InvalidSubmission('submission is not an image.')

		timestamp = imageurl.split('/')[5]  # this will ALWAYS be [5]
		if isint(timestamp) is not None :
			timestamp = int(timestamp)
			uploadTimestamp = imageurl.split('/')[6]
			uploadTimestamp = int(uploadTimestamp[:uploadTimestamp.find('.')])
		elif timestamp in FurAffinityCrawler.submissionTypes :
			raise InvalidSubmission(f'submission is not an image. type: {timestamp}.')
		else :
			raise BadOrMalformedResponse(f'could not find image id (timestamp) from image url. imageurl: {imageurl}, timestamp: {timestamp}.')

		if imageurl.startswith('//') :
			imageurl = 'https:' + imageurl

		artist = first(document.xpath('//div[@class="submission-id-container"]//a[contains(@href, "/user/") and strong]', **self.xpathargs))
		if artist :
			artisturl = first(artist.xpath('@href', **self.xpathargs))
			if artisturl :
				artisturl = 'https://www.furaffinity.net' + artisturl
			else :
				raise BadOrMalformedResponse('could not find artist url in html.')

			artist = first(artist.xpath('strong/text()', **self.xpathargs))
		if not artist :
			raise BadOrMalformedResponse('could not find artist in html.')


		sidebar = first(document.xpath('//div[@class="submission-sidebar"]', **self.xpathargs))

		tags = sidebar.xpath('self::*//section[@class="tags-row"]//span[@class="tags"]/a/text()', **self.xpathargs)
		tags += sidebar.xpath('self::*//section[@class="info text"]//span[preceding-sibling::strong[not(contains(text(), "Size"))]]/text()', **self.xpathargs)
		tags += sidebar.xpath('self::*//section[@class="info text"]/div/div[preceding-sibling::strong[not(contains(text(), "Size"))]]//span/text()', **self.xpathargs)

		rating = first(sidebar.xpath('self::*//div[@class="rating"]//span[contains(@class, "rating-box")]/text()', **self.xpathargs))
		rating = str(rating).strip()

		resolution = first(sidebar.xpath('self::*//section[@class="info text"]//span[contains(preceding-sibling::*/text(), "Size")]/text()', **self.xpathargs))
		if resolution :
			resolution = resolution.split('x')
			x = isint(resolution[0])
			y = isint(resolution[1])
			if x and y :
				resolution = (x, y)
			else :
				resolution = None

		title = first(document.xpath('//div[@class="submission-id-container"]//div[@class="submission-title"]//p/text()', **self.xpathargs))
		if not title :
			self.logger.warning(f'could not find submission title in html. url: {self.url}')

		# for furaffinity crawls, self.url holds the webcode
		thumbnail = f'https://t.facdn.net/{self.url}@{{}}-{timestamp}.jpg'
		thumbnails = [thumbnail.format(r) for r in (200, 300, 400, 600, 800)]

		values = document.xpath('//div[@class="online-stats"]/text()', **self.xpathargs)
		keys = document.xpath('//div[@class="online-stats"]/strong/text()', **self.xpathargs)
		users = dict(zip(keys, list(map(int, filter(None, map(lambda x : x.strip(' \t\n\r\x0b\x0c,.and-—'), values))))[1:]))
		if not users :
			raise BadOrMalformedResponse('could not find registered users online in html.')
		if users['registered'] > 10000 :
			self.sleepfor = 30
		self.logUsers(users)

		return {
			'url': self.formattedurl,
			'imageurl': imageurl,
			'id': self.url,
			'resolution': resolution,
			'artist': artist,
			'artisturl': artisturl,
			'title': title,
			'rating': rating,
			'tags': tags,
			'timestamp': uploadTimestamp,
			'thumbnails': thumbnails,
			'website': 'furaffinity',
		}


	@SimpleCache(300)
	def logUsers(self, users) :
		self.logger.info(users)
