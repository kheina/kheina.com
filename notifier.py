from common.crawling import normalizeTag
from common.MessageQueue import Receiver
from common.caching import SimpleCache
from re import compile as re_compile
from pycrawl.common import isint
from listener import deleteUser
from urllib.parse import quote
from common.cwd import SetCWD
from common import logging
import ujson as json
import asyncio
import aiohttp
import pika
import time
import sys
import os


# fetch working directory
SetCWD()
logger = logging.getLogger('notifier', disable=['pika'])


class Notifier(Receiver) :

	TitleTags = re_compile('('+'|'.join([
		'ych', 'reminder', 'raffle', 'adopt',
		# 'sale', 'important', 'open', 'closed', 'contest',
		'[a-z]+',
		# '\w+',
	])+')')

	htmlreplace = re_compile(r'[<>&]')
	htmlmap = {
		'<': '&lt;',
		'>': '&gt;',
		'&': '&amp;',
	}

	deleteUserErrors = {
		403
	}


	def __init__(self, looptime=5, ttl=float('inf'), dedupe=5) :
		self.looptime = looptime
		self.TTL = time.time() + ttl
		self._dedupe = set(range(dedupe))

		with open('credentials/notifier.json') as credentials :
			credentials = json.load(credentials)
			self._connection_info = credentials['connection_info']
			self._exchange_info = credentials['exchange_info']
			self._channel_info = credentials['channel_info']
			self._route = credentials['routing_key']
			self._telegram_access_token = credentials['telegram_access_token']
			self._telegram_bot_id = credentials['telegram_bot_id']


	def url_encode(self, text) :
		return quote(Notifier.htmlreplace.sub(lambda x : Notifier.htmlmap[x.group(0)], text))


	async def filter(self, hash) :
		if hash in self._dedupe :
			return False

		# set should rotate the items on its own
		self._dedupe.pop()
		self._dedupe.add(hash)
		return True


	async def sendMessage(self, recipient, message) :
		# mode = MarkdownV2 or HTML
		request = f'https://api.telegram.org/bot{self._telegram_access_token}/sendMessage?chat_id={recipient}&parse_mode=HTML&text='
		url = request + message
		errorMessage = 'failed to send notification to telegram.'
		info = None
		for _ in range(5) :
			try :
				async with aiohttp.request('GET', url) as response :
					info = json.loads(await response.text())
					if not info['ok'] :
						break
					return True
			except :
				pass

		if info and info.get('error_code') in deleteUserErrors :
			logger.warning({
				'info': info,
				'message': 'deleting user profile.',
			})
			deleteUser(recipient)
		else :
			logger.error({
				'info': info,
				'message': errorMessage,
				'request': request,
				'telegram message': message,
			})
		return False


	async def alert(self, item) :
		userMap = Notifier.getTagTree()

		# add a bunch of extra tags we can pull from the item fields
		tags = item['tags'] + [
			normalizeTag(item['artist']),
			item['artist'].lower()+'/a',
			item['rating'].lower(),
			item['rating'].lower()+'/r',
		]
		title = item['title']
		if title :
			# nothing returned from title tags should need tag normalization
			tags += Notifier.TitleTags.findall(title.lower())

		tree = TreeTraverser(tags)
		del tags

		# do the things
		tree.blupdate(userMap.blacklist)
		tree.update(userMap.whitelist)

		# if no one's going to receive the message, just return early
		if not tree.recipients :
			return

		# make sure we haven't sent alerts for this image recently
		if not await self.filter(item['sha1']) :
			return

		title = '<b>' + self.url_encode(title) + '</b>' if title else 'new upload'

		message_p1 = title + ' by ' + item['artist'] + ' matched your tags: <i>'
		message_p2 = f'</i>\n<a href="{item["imageurl"]}">sample</a>, <a href="{item["url"]}">source</a>, <a href="{item["artisturl"]}">artist</a>'

		count = 0
		for recipient, path in tree.recipients.items() :
			count += await self.sendMessage(recipient, message_p1 + path + message_p2)
		logger.info({
			'count': count,
			'whitelist': len(tree.recipients),
			'blacklist': len(tree.blacklist),
		})


	@SimpleCache(60)
	def getTagTree() :
		userData = Notifier.pullUserData()
		processed = TreeRoot()

		for user, prefs in userData.items() :
			# for list_type in TreeRoot.list_types :
			# whitelist first
			for tags in prefs.get('whitelist', []) :
				Notifier.addTagsToTree(processed.whitelist, tags, user)

			# now blacklist
			for tags in prefs.get('blacklist', []) :
				Notifier.addTagsToTree(processed.blacklist, tags, user)

		# traverse the tree to see if it can be simplified anywhere
		"""
		for list_type, tree in processed.items() :

			for root, content in tree.items() :
				content.keys()
		"""

		logger.info(f'consumed preferences for {len(userData)} users.')
		return processed


	def pullUserData() :
		return {
			file[:-5]: json.load(open('notifier/data/'+file))
			for file in os.listdir('notifier/data')
			if file.endswith('.json') and isint(file[:-5])
		}


	def addTagsToTree(tree, tags, user) :
		for tag in tags :
			match = True
			if tag.startswith('!') :
				match = False
				tag = tag[1:]

			if match :
				if not tree.match :
					tree.match = { }
				tree = tree.match

			else :
				if not tree.nomatch :
					tree.nomatch = { }
				tree = tree.nomatch

			if tag not in tree :
				tree[tag] = TreeNode()
			tree = tree[tag]

		if not tree.users :
			tree.users = set()
		tree.users.add(int(user))


	async def run(self) :

		while True :
			starttime = time.time()
			metadata = self.recv(kwargs.get('forcelist'))

			count = 0
			for item in metadata :
				try :
					# 'queue' the tasks for completion asynchronously
					# asyncio.ensure_future(self.alert(item))
					await self.alert(item)

				except :
					logger.exception({
						'message': 'notification failed.',
						'item': item,
					})

				finally :
					count += 1

			endtime = time.time()
			elapsedtime = endtime - starttime
			sleepfor = self.looptime - elapsedtime
			if sleepfor + endtime > self.TTL :
				return
			await asyncio.sleep(sleepfor)


	def recv(self, forcelist=False) :
		# just let it fail if it's not json serialized
		if forcelist :
			return list(map(json.loads, self._recv()))
		else :
			return map(json.loads, self._recv())


class TreeTraverser :

	def __init__(self, tags) :
		self.recipients = { }
		self.blacklist = set()
		self.tags = set(tags)

	"""
	def recipients(self) :
		for recipient in self._recipients.keys() - self.blacklist :
			yield recipient, self._recipients[recipient]
	"""

	# assume the blacklist is fully-formed
	def update(self, tree) :
		# eliminate as many keys immediately as possible, then iterate over them
		if tree.match :
			for key in tree.match.keys() & self.tags :
				self._update(tree.match[key], key)

		if tree.nomatch :
			for key in tree.nomatch.keys() - self.tags :
				self._update(tree.nomatch[key], '!' + key)

	def _update(self, tree, path) :
		# we can either remove the blacklist members here, or do it afterward
		# an analysis should be done on which method is faster
		# self.recipients.update({ k: path for k in tree.users - self.blacklist })
		if tree.users :
			for user in tree.users - self.blacklist - self.recipients.keys() :
				self.recipients[user] = path

		if tree.match :
			for key in tree.match.keys() & self.tags :
				self._update(tree.match[key], path + ', ' + key)

		if tree.nomatch :
			for key in tree.nomatch.keys() - self.tags :
				self._update(tree.nomatch[key], path + ', !' + key)

	# run this first
	def blupdate(self, tree) :
		# eliminate as many keys immediately as possible, then iterate over them
		if tree.match :
			for key in tree.match.keys() & self.tags :
				self._blupdate(tree.match[key])

		if tree.nomatch :
			for key in tree.nomatch.keys() - self.tags :
				self._blupdate(tree.nomatch[key])

	def _blupdate(self, tree) :
		if tree.users :
			self.blacklist.update(tree.users)

		if tree.match :
			for key in tree.match.keys() & self.tags :
				self._blupdate(tree.match[key])

		if tree.nomatch :
			for key in tree.nomatch.keys() - self.tags :
				self._blupdate(tree.nomatch[key])


class TreeRoot :
	list_types = ['whitelist', 'blacklist']
	def __init__(self) :
		self.whitelist = TreeNode()
		self.blacklist = TreeNode()

	def dict(self) :
		return { 'whitelist': self.whitelist.dict(), 'blacklist': self.blacklist.dict() }


class TreeNode :

	def __init__(self) :
		self.users = None
		self.match = None
		self.nomatch = None

	def dict(self) :
		return {
			'users': list(self.users) if self.users else None,
			'match': { k: v.dict() for k, v in self.match.items() } if self.match else None,
			'nomatch': { k: v.dict() for k, v in self.nomatch.items() } if self.nomatch else None,
		}


if __name__ == '__main__' :
	from ast import literal_eval
	kwargs = { k: literal_eval(v) for k, v in (arg.split('=') for arg in sys.argv[1:]) }
	try :
		notifier = Notifier(**kwargs)
		loop = asyncio.get_event_loop()
		# Blocking call which returns when the hello_world() coroutine is done
		loop.run_until_complete(notifier.run())
		time.sleep(5)
		loop.close()
	finally :
		pass
