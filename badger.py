import sys
import subprocess

def getcommits() :
	try : return int(subprocess.check_output(['git', 'rev-list', '--count', 'HEAD']))
	except : return -1

def createbadge(template, filename) :
	commits = getcommits()
	svg = ''
	with open(template, 'r') as image : svg = image.read()
	index = svg.find('<text')
	if index < 0 : return None
	index = svg.find('>', index+5) + 1
	if index <= 0 : return None
	svg = svg[:index] + str(commits) + svg[index:]
	with open(filename, 'w') as image : image.write(svg)
	return filename

if __name__ == '__main__' :
	if len(sys.argv) == 3 : print(createbadge(sys.argv[1], sys.argv[2]))
	else : print('requires exactly 2 arguements: createbadge(template, filename)')