try : import ujson as json
except : import json
import parsesource
import requests
import hashlib
import time
import sys

def FNgetmeta(imagemeta, metafilename='FNmeta.txt') :
	url = 'https://furrynetwork.com/artwork/' + str(imagemeta['id'])
	image = imagemeta['images']['original']
	id = imagemeta['id']
	resolution = parsesource.getimageinfofromurl(image)
	if resolution is not None : resolution = {'x':resolution[0], 'y':resolution[1]}
	artist = imagemeta['character']['display_name']
	artisturl = 'https://furrynetwork.com/' + imagemeta['character']['name'] + '/'
	title = imagemeta['title']
	rating = imagemeta['rating']
	if rating == 0 : rating = 'General'
	elif rating == 1 : rating = 'Mature'
	elif rating == 2 : rating = 'Adult'
	tags = []
	for tag in imagemeta['tags'] :
		if tag['type'] == 'artist' :
			tags.append(tag['value'])
	date = imagemeta['published']

	thumbnails = [imagemeta['images']['small'],imagemeta['images']['medium'],imagemeta['images']['large']]
	
	meta = {'url':url, 'imageurl':image, 'id':id, 'resolution':resolution, 'artist':artist, 'artisturl':artisturl, 'title':title, 'rating':rating, 'tags':tags, 'date':date, 'thumbnails':thumbnails, 'website':'Furry Network'}
	try : B2return = parsesource.downloadimagefordatabase(imagemeta['images']['medium'])
	except :
		print('FN id', meta['id'], 'failed')
		return None
	if B2return is None : return meta['id']
	else :
		sha1, B2return = B2return
		meta['sha1'] = sha1
		meta['filename'] = 'images/' + sha1 + '.jpg'
		if B2return is None :
			with open('B2errors.txt', 'a') as B2errors :
				B2errors.write(str(meta['id']) + ' ' + meta['filename'] + '\n')
		with open(metafilename, 'a') as metafile :
			metafile.write(json.dumps(meta,escape_forward_slashes=False) + '\n')
		return True
	return None



def scoopsubmissions(ids, metafilename='FNmeta.txt') :
	try :
		rangelen = len(ids)-1
		for i in range(rangelen,0,-1) :
			#print('id:', ids[i]['id'], end=' ', flush= True)
			response = parsesource.FNparse(ids[i]['id'])
			if b'message' in response :
				ids[i]['attempts'] = ids[i]['attempts'] + 1
				if ids[i]['attempts'] >= 3 :
					del ids[i]
					#print('failed, removing')
				#else : print('failed, skipping')
			else :
				FNgetmeta(response, metafilename=metafilename)
				del ids[i]
				#print('done.')
	finally :
		return ids

def scoopnewsubmissions(startingid=None, runtime=float('inf'), loadskipped=False) :
	startingid = parsesource.isint(startingid)
	if not startingid :
		with open('FNstartingid.txt', 'r') as startid :
			startingid = int(startid.read())
	initialtime = time.time()
	checkevery = 300  # check the skipped after this many seconds
	skips = 0
	skipped=[]
	tempskipped = []
	if loadskipped :
		with open('FNstartingchecks.txt', 'r') as checks :
			skipped = json.load(checks)
	print('FN skips:', len(skipped))
	sleepfor = 0
	runtime = runtime + time.time()
	try :
		while runtime > time.time() :
			if sleepfor > 0 : time.sleep(sleepfor)
			sleepfor = 0
			#print('id:', startingid, end=' ', flush= True)
			response = parsesource.FNparse(startingid)
			if b'message' in response :
				skips = skips + 1
				if not parsesource.contains(skipped, lambda x: x['id'] == startingid) : tempskipped.append({'id':startingid, 'attempts':1})
				if skips >= 250 :
					sleepfor = 60
					startingid = startingid - skips
					#print('encountered', skips, 'skips and sleeping for', sleepfor, 'seconds (', startingid, '-', startingid+skips, ')')
					skips = 0
					tempskipped = []
					with open('FNstartingid.txt', 'w') as startid :
						startid.write(str(startingid))
				else :
					#print(response)
					startingid = startingid + 1
			elif b'error' in response : exit(1)
			else :
				FNgetmeta(response)
				index = parsesource.contains(skipped, lambda x: x['id'] == startingid)
				if index : del skipped[index]
				#print('done.')
				startingid = startingid + 1
				skips = 0
				if len(tempskipped) :
					skipped = skipped + tempskipped
					tempskipped = []
				with open('FNstartingid.txt', 'w') as startid :
					startid.write(str(startingid))
			if time.time() - initialtime > checkevery :
				print(checkevery, 'seconds have elapsed, checking skipped submissions...')
				submissions = len(skipped)
				timeelapsed = time.time()
				skipped = scoopsubmissions(skipped)
				timeelapsed = time.time() - timeelapsed
				submissions = submissions - len(skipped)
				print('confirmed', str(submissions) + '/' + str(len(skipped)), 'FN submissions in', round(timeelapsed, 2), 'seconds.')
				initialtime = time.time()
	finally :
		with open('FNstartingid.txt', 'w') as startid :
			startid.write(str(startingid))
		#print('\nskips (you may want to double check these):\n', skipped)
		with open('FNstartingchecks.txt', 'w') as checks :
			json.dump(skipped, checks)

def scoopoldsubmissions(startingid=None, runtime=float('inf'), loadskipped=False) :
	startingid = parsesource.isint(startingid)
	if not startingid :
		with open('FNbacklog.txt', 'r') as startid :
			startingid = int(startid.read())
	initialtime = time.time()
	checkevery = 300 # check the skipped after this many seconds
	skips = 0
	skipped = []
	if loadskipped :
		with open('FNbacklogchecks.txt', 'r') as checks :
			skipped = json.load(checks)
	print('FN old skips:', len(skipped))
	runtime = runtime + time.time()
	try :
		while runtime > time.time() :
			#print('id:', startingid, end=' ', flush= True)
			response = parsesource.FNparse(startingid)
			if b'message' in response :
				skips = skips + 1
				if not parsesource.contains(skipped, lambda x: x['id'] == startingid) : skipped.append({'id':startingid, 'attempts':1})
				#print(response)
				startingid = startingid - 1
			elif b'error' in response : exit(1)
			else :
				FNgetmeta(response, metafilename='FNmetaold.txt')
				index = parsesource.contains(skipped, lambda x: x['id'] == startingid)
				if index : del skipped[index]
				#print('done.')
				startingid = startingid - 1
				skips = 0
				with open('FNbacklog.txt', 'w') as startid :
					startid.write(str(startingid))
			if time.time() - initialtime > checkevery :
				print(checkevery, 'seconds have elapsed, checking skipped submissions...')
				submissions = len(skipped)
				timeelapsed = time.time()
				skipped = scoopsubmissions(skipped, metafilename='FNmetaold.txt')
				timeelapsed = time.time() - timeelapsed
				submissions = submissions - len(skipped)
				print('confirmed', str(submissions) + '/' + str(len(skipped)), 'FN submissions in', round(timeelapsed, 2), 'seconds.')
				initialtime = time.time()
	finally :
		with open('FNbacklog.txt', 'w') as startid :
			startid.write(str(startingid))
		#print('\nskips (you may want to double check these):\n', skipped)
		with open('FNbacklogchecks.txt', 'w') as checks :
			json.dump(skipped, checks)

if __name__ == '__main__' :
	parsesource.start()
	o = False
	id = None
	for i in range(1, len(sys.argv)) :
		if sys.argv[i] == 'old' : o = True
		elif parsesource.isint(sys.argv[i]) :
			id = int(sys.argv[i])
	if o : scoopoldsubmissions(id)
	else : scoopnewsubmissions(id)
