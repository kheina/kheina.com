from starlette.responses import HTMLResponse
from re import compile as re_compile
from collections import defaultdict
try : import ujson as json
except : import json
import time

class Template(dict) :
	convert = defaultdict(lambda : str, {
		dict: json.dumps,
		list: json.dumps,
		tuple: json.dumps,
	})

	def getReplace(self, match) :
		field = self.get(match.group(1))
		if field :
			return Template.convert[type(field)](field)
		return match.group(0)

class HtmlTemplate :
	regexSub = re_compile(r'{(\w+?)}')
	regexTemplate = re_compile(r'(?:^|\n)<(\w+?)>((?:\n|.)*?)\n<\/\1>')

	def __init__(self, TTL=300) :
		self.TTL = TTL
		self.expire = 0
		self.template = { }

	async def loadTemplateData(self, file='template.html') :
		with open(file) as template :
			template = template.read()
			for key, html in HtmlTemplate.regexTemplate.findall(template) :
				self.template[key] = html.strip()
		self.expire = time.time() + self.TTL

	async def format(self, filename, status_code=200, headers=None, **kwargs) :
		with open(filename) as text :
			string = text.read()
		if time.time() > self.expire :
			await self.loadTemplateData()
		kwargs = Template(self.template, **kwargs)
		return HTMLResponse(HtmlTemplate.regexSub.sub(kwargs.getReplace, string), status_code=status_code, headers=headers)
