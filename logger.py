agent = None

class FakeLogger :
	loggable = { str, int, float }

	def __init__(self) :
		import time
		self.time = time

	def flatten(self, it) :
		if issubclass(type(it), (tuple, list, set)) :
			for i in it :
				yield from self.flatten(i)
		elif issubclass(type(it), dict) :
			for k, v in it.items() :
				yield from self.flatten(v)
		else :
			yield it

	def log_text(self, log, severity='INFO') :
		print('[' + self.time.asctime(self.time.localtime(self.time.time())) + ']', severity, '>', log)

	def log_struct(self, log, severity='INFO') :
		for i in self.flatten(log) :
			if type(i) not in FakeLogger.loggable :
				print('WARNING:', i, 'may not be able to be logged.')
		print('[' + self.time.asctime(self.time.localtime(self.time.time())) + ']', severity, '>', log)

def start(loggerName='default') :
	global agent
	try :
		from google.auth import compute_engine
		from google.cloud import logging
		credentials = compute_engine.Credentials()
		logging_client = logging.Client(credentials=credentials)
		agent = logging_client.logger(loggerName)
	except :
		agent = FakeLogger()
	return agent

def log(log, severity='INFO') :
	if isinstance(log, dict) :
		agent.log_struct(log, severity=severity)
	elif isinstance(log, str) :
		agent.log_text(log, severity=severity)
	else :
		agent.log_text(str(log), severity=severity)
