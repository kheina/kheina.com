import authenticator

class Row() :
	def __init__(self, username, passwordHash, passwordSalt, emailHash, recoveryEmailUsername, recoveryEmailDomain, patronStatus=0) :
		self.username = username
		self.passwordHash = passwordHash
		self.passwordSalt = passwordSalt
		self.emailHash = emailHash
		self.recoveryEmailUsername = recoveryEmailUsername
		self.recoveryEmailDomain = recoveryEmailDomain
		self.patronStatus = patronStatus

class MockUserDatabase() :
	def __init__(self) :
		self._db = []
		self._index = { }
		self._keyChain = { }
	
	def addUser(self, email, username, password) :
		pwordToken = authenticator.hash(password)
		emailToken = authenticator.hash(email, salt=b'yummy biscuits in yo face hole~')
		recovery = email.split('@')
		recovery[0] = recovery[0][:3]
		self._db.append(Row(username, pwordToken._hash, pwordToken._salt, emailToken._hash, recovery[0], recovery[1]))
		self._index[emailToken._hash] = len(self._db) - 1

	def verifyLogin(self, email, password) :
		emailToken = authenticator.hash(email, salt=b'yummy biscuits in yo face hole~')
		index = self._index.get(emailToken._hash)
		if index is None : return
		user = self._db[index]
		passwordToken = authenticator.hash(password, salt=user.passwordSalt)
		if user.passwordHash == passwordToken._hash :
			key = authenticator.generateKey()
			self._keyChain[key._login] = (emailToken._hash, key)
			return key, user.username

	def verifyKey(self, loginKey, refreshKey=None, TTL=86400) :
		key = self._keyChain.get(key._login)
		if not key : return None
		emailHash, key = key
		newKey = key.verifyLogin(loginKey, refreshKey=refreshKey)
		index = self._index.get(emailHash)
		if index is None : return None
		return (newKey if newKey else key, self._db[index])
