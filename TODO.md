## todo list, in no particular order
- implement async IO in [databaser.queryiqdb](https://gitlab.com/kheina/kheina.com/wikis/databaser.py#queryiqdb) and [databaser.querypostgres](https://gitlab.com/kheina/kheina.com/wikis/databaser.py#querypostgres) (to a lesser degree [parsesource.parseformdata](https://gitlab.com/kheina/kheina.com/wikis/parsesource.py#parseformdata) and [parsesource.loadimagedata](https://gitlab.com/kheina/kheina.com/wikis/parsesource.py#loadimagedata) as well)
- analytics page (with real-time data after each database update)
- custom error pages (400, 401, 403, 404, 500)
- batch searching (searching many files at once)
- user accounts (for patron features like batch searching)
- integrated searched (search other websites and results into one page)
