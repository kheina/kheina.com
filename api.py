from starlette.responses import UJSONResponse, FileResponse, PlainTextResponse
from pycrawl.common import GetFullyQualifiedClassName
from HtmlTemplate import HtmlTemplate
from common import HTTPError, logging
from common.safejoin import SafeJoin
from urllib.parse import urlparse
from traceback import format_tb
from common.cwd import SetCWD
import ujson as json
import parsesource
import databaser
import badger
import time
import sys
import os


SetCWD()
staticDirectory = 'static'
logger = logging.getLogger('api')


async def handle_json(value) :
	try :
		with open(SafeJoin(value + '.json'), 'r') as jason :
			return jason.read()
	except FileNotFoundError :
		raise HTTPError.NotFound('The requested resource is not available.')


################################################## API ##################################################
apiUrls = { '127.0.0.1', 'api.kheina.com' }
allowed_hosts = { '127.0.0.1', 'kheina.com', 'beta.kheina.com', 'api.kheina.com' }
apiHeaders = {
	'content-type': 'application/json',
	'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
	'Access-Control-Max-Age': '2592000',
	'Access-Control-Allow-Headers': 'origin, x-csrf-token, content-type, accept, user-agent, referer, authorization, cookie'
}

async def JSONErrorHandler(req) :
	exc_type, e, exc_tb = sys.exc_info()
	status = getattr(e, 'status', 500)

	error = {
		'error': f'{status} {GetFullyQualifiedClassName(e)}: {e}',
		'status': status,
		'stacktrace': format_tb(exc_tb),
		'method': req.method,
		'url': str(req.url),
		**getattr(e, 'logdata', { }),
	}
	logger.error(error)
	return UJSONResponse(
		error,
		status_code=status,
		headers={
			'Access-Control-Allow-Origin': req.headers.get('origin', 'null'),
			**apiHeaders,
		})

async def v1origins(req) :
	try :
		if req.url.netloc not in apiUrls :
			raise HTTPError.Forbidden('API is not accessible from this URL.')
		origin = req.headers['origin']
		if urlparse(origin).netloc not in allowed_hosts :
			raise HTTPError.BadRequest(
				'your request was bad, and you should feel bad.',
				logdata={
					'origin': origin,
					'referer': req.headers.get('referer'),
				},
			)

		return UJSONResponse({
			'error': None,
		}, headers={
			'Access-Control-Allow-Origin': origin,
			**apiHeaders,
		})
	except :
		return await JSONErrorHandler(req)

async def v1search(req) :
	# key, user = authServer.verifyKey(req.session.get('login-key'), refreshKey=req.session.get('refresh-key'))
	# validate the request came from api.kheina.com

	try :
		if req.url.netloc not in apiUrls :
			raise HTTPError.Forbidden('API is not accessible from this URL.')
		origin = req.headers.get('origin')
		if not origin :
			origin = 'null'
		elif urlparse(origin).netloc not in allowed_hosts :
			raise HTTPError.BadRequest(
				'your request was bad, and you should feel bad.',
				logdata={
					'origin': origin,
					'referer': req.headers.get('referer'),
				},
			)

		log = { }
		stats = await handle_json('stats')
		formdata = await req.form()

		if 'file' in formdata :
			imagedata = formdata['file'].file.read()
		elif 'url' in formdata :
			log['imageurl'] = formdata['url']
			imagedata = parsesource.loadimagedata(formdata['url'])
		else : raise HTTPError.BadRequest('No form data provided.')

		if not imagedata :
			logdata = { k: v[:100] for k, v in formdata.items() }
			raise HTTPError.BadRequest(f'Form data was not able to be processed: {", ".join(list(logdata.keys()))}.', logdata=logdata)

		initialtime = time.time()
		results = databaser.queryiqdb(imagedata)
		elapsedtime = time.time() - initialtime
		log.update({
			'method': req.method,
			'url': str(req.url),
			'similarity': results[0]['similarity'],
			'elapsedtime': elapsedtime,
		})
		logger.info(log)
		return UJSONResponse({
			'results': results,
			'elapsedtime': elapsedtime,
			'stats': json.loads(stats),
			'error': None,
		}, headers={
			'Access-Control-Allow-Origin': origin,
			**apiHeaders,
		})
	except :
		return await JSONErrorHandler(req)

async def v1user(req) :
	# key, user = authServer.verifyKey(req.session.get('login-key'), refreshKey=req.session.get('refresh-key'))
	# validate the request came from api.kheina.com

	try :
		if req.url.netloc not in apiUrls :
			raise HTTPError.Forbidden('API is not accessible from this URL.')
		origin = req.headers.get('origin')
		if not origin :
			origin = 'null'
		elif urlparse(origin).netloc not in allowed_hosts :
			raise HTTPError.BadRequest(
				'your request was bad, and you should feel bad.',
				logdata={
					'origin': origin,
					'referer': req.headers.get('referer'),
				},
			)

		log = { }
		stats = await handle_json('stats')
		requestJson = await req.json()
		user = requestJson['user']
		pagenum = requestJson.get('page', 0)
		count = requestJson.get('count', 24)

		initialtime = time.time()
		results = databaser.queryartist(user, pagenum, count)
		elapsedtime = time.time() - initialtime
		log.update({
			'method': req.method,
			'url': str(req.url),
			'elapsedtime': elapsedtime,
		})
		logger.info(log)
		return UJSONResponse({
			'results': results,
			'elapsedtime': elapsedtime,
			'stats': json.loads(stats),
			'error': None,
		}, headers={
			'Access-Control-Allow-Origin': origin,
			**apiHeaders,
		})
	except :
		return await JSONErrorHandler(req)

async def v1image(req) :
	# key, user = authServer.verifyKey(req.session.get('login-key'), refreshKey=req.session.get('refresh-key'))
	# validate the request came from api.kheina.com

	try :
		if req.url.netloc not in apiUrls :
			raise HTTPError.Forbidden('API is not accessible from this URL.')
		origin = req.headers.get('origin')
		if not origin :
			origin = 'null'
		elif urlparse(origin).netloc not in allowed_hosts :
			raise HTTPError.BadRequest(
				'your request was bad, and you should feel bad.',
				logdata={
					'origin': origin,
					'referer': req.headers.get('referer'),
				},
			)

		stats = await handle_json('stats')
		requestJson = await req.json()
		sha1 = requestJson['sha1']

		initialtime = time.time()
		results = databaser.queryimage(sha1)
		elapsedtime = time.time() - initialtime
		logger.info({
			'method': req.method,
			'url': str(req.url),
			'elapsedtime': elapsedtime,
		})
		return UJSONResponse({
			'results': results,
			'elapsedtime': elapsedtime,
			'stats': json.loads(stats),
			'error': None,
		}, headers={
			'Access-Control-Allow-Origin': origin,
			**apiHeaders,
		})
	except :
		return await JSONErrorHandler(req)

async def v0stats(req) :
	try :
		origin = req.headers.get('origin')
		if not origin :
			origin = 'null'
		elif urlparse(origin).netloc not in allowed_hosts :
			raise HTTPError.BadRequest(
				'your request was bad, and you should feel bad.',
				logdata={
					'origin': origin,
					'referer': req.headers.get('referer'),
				},
			)

		return PlainTextResponse(
			await handle_json('stats'),
		headers={
			'Access-Control-Allow-Origin': origin,
			**apiHeaders,
		})
	except :
		return await JSONErrorHandler(req)



async def startup() :
	databaser.start(readonly=True)
	logger.info('server started')

async def shutdown() :
	databaser.cleanup()

from starlette.applications import Starlette
from starlette.staticfiles import StaticFiles
from starlette.middleware import Middleware
from starlette.middleware.trustedhost import TrustedHostMiddleware
from starlette.routing import Route, Mount

middleware = [
    Middleware(TrustedHostMiddleware, allowed_hosts=allowed_hosts)
]

routes = [
	Route('/stats', endpoint=v0stats),
	Route('/v1/search', endpoint=v1search, methods=('POST',)), 	# /v1/search
	Route('/v0/user', endpoint=v1user, methods=('POST',)), 		# /v1/user
	Route('/v0/image', endpoint=v1image, methods=('POST',)), 	# /v1/image
	Route('/v{location:path}', endpoint=v1origins, methods=('OPTIONS',)),
]

app = Starlette(
	routes=routes,
	middleware=middleware,
	on_startup=[startup],
	on_shutdown=[shutdown],
)

if __name__ == '__main__' :
	from uvicorn.main import run
	run(app, host='127.0.0.1', port=80)

badger.createbadge('server-badge-template.svg', staticDirectory + '/badges/server.svg')
