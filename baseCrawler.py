from pycrawl.crawler import Crawler, BaseCrawlingException, ShutdownCrawler, InvalidResponseType
from pycrawl.common import GetFullyQualifiedClassName
from common.crawling import normalizeTag
from hashlib import sha1 as hashlib_sha1
from common.HTTPError import HTTPError
from importlib import import_module
from databaser import validateSha1
from traceback import format_tb
from base64 import b64encode
from common import logging
from PIL import ImageFile
import ujson as json
import requests
import time
import sys


class ImageDownloadError(BaseCrawlingException, HTTPError) :
	def __init__(self, message, logdata={ }, status=500) :
		BaseCrawlingException.__init__(self, message, logdata)
		HTTPError.__init__(self, message, status)


mimeTypeMap = {
	# type: (extension, mimetype)
	'JPEG': ('jpg', 'image/jpeg'),
	'GIF': ('gif', 'image/gif'),
	'PNG': ('png', 'image/png'),
	'WEBP': ('webp', 'image/webp'),  # double check this pillow type (the key)
}


ratingmap = {
	'general': 'general',
	'mature': 'mature',
	'adult': 'explicit',
	'explicit': 'explicit',
}


class BaseCrawler(Crawler) :

	def __init__(self, *args, key=None, domain=None, **kwargs) :
		"""
		imageMaxRetries: how many times to try and download an image before adding to skips
		b2cutoff: how many images are allowed in the B2 queue before crawler sleeps or shuts down
		imagetimeout: how long to wait when downloading an image
		b2maxretries: number of times to retry uploading an image to B2 before giving up (images will enter a queue to retry)
		route: routing key to use for message queue publishing
		"""
		super().__init__(*args, **kwargs)
		self.errorHandlers.update({
			ImageDownloadError: self.imageDownloadErrorHandler,  # we know there's a submission here, it was crawled, but the image can't be downloaded for some reason
		})
		self.doNotLog.add(ImageDownloadError)
		self.B2MaxRetries = int(kwargs.get('b2maxretries', 5))
		self.imageTimeout = float(kwargs.get('imagetimeout', 60))
		self.B2cutoff = int(kwargs.get('b2cutoff', 25))
		self.imageMaxRetries = int(kwargs.get('imagemaxretries', 2))
		self.B2skips = []
		self.imageDownloadSkips = []
		self.unblocking.update({
			503: self.unblockCF
		})
		self.logger = logging.getLogger(self.name.replace('+', '.'))
		self.thumbnailIndex = -1

		# redefine skipped as a list because google logging shits itself when you log a tuple
		self.skipped = list(self.skipped)

		# initialize B2
		self.B2 = None

		try :
			print('loading credentials...', end='', flush=True)
			with open('credentials/crawlers.json', 'r') as credentials :
				credentials = json.load(credentials)

				if key :
					# prime the session with headers and cookies
					self._session.headers.update(credentials[key]['headers'])

					cookies = credentials[key]['cookies']

					# manually load cookies into the session
					for name, value in cookies.items() :
						self._session.cookies.set(name=name, value=value, domain=domain)

				# authorize with B2
				if not self._authorizeB2(credentials['B2']) :
					raise ShutdownCrawler(f'B2 authorization handshake failed on startup.')

				# message queue setup
				if 'message_queue' not in credentials :
					raise ShutdownCrawler(f'message queue initialization failed on startup.')

				credentials = credentials["message_queue"]

				# update the default routing key with the passed one, should it exist
				if 'route' in kwargs :
					credentials['publish_info']['routing_key'] = kwargs['route']

				# establish initial connection to message queue
				self.mqConnect(credentials['connection_info'], credentials['exchange_info'], credentials['publish_info'])
			print('success.')

		except Exception as e :
			exc_type, exc_obj, exc_tb = sys.exc_info()
			self.logger.error({
				'error': f'{GetFullyQualifiedClassName(e)}: {e}',
				'stacktrace': format_tb(exc_tb),
				'id': self.id,
				'name': self.name,
				**getattr(e, 'logdata', { }),
			})
			raise ShutdownCrawler(e).with_traceback(exc_tb)

		self.logger.info(f'{self.name} initialized.')


	def _authorizeB2(self, B2credentials=None) :
		try :
			if not B2credentials :
				print('loading credentials...', end='', flush=True)
				with open('credentials/crawlers.json', 'r') as credentials :
					credentials = json.load(credentials)
					B2credentials = credentials['B2']
				print('success.')

			basic_auth_string = b'Basic ' + b64encode((B2credentials['keyId'] + ':' + B2credentials['key']).encode())
			B2headers = { 'Authorization': basic_auth_string }
			response = requests.get('https://api.backblazeb2.com/b2api/v2/b2_authorize_account', headers=B2headers, timeout=self.timeout)

		except :
			self.logger.error(self.crashInfo())

		else :
			if response.ok :
				self.B2 = json.loads(response.content)
				return True

			else :
				self.logger.error(f'B2 authorization handshake failed: {response.content}')

		return False


	def crashInfo(self) :
		ci = Crawler.crashInfo(self)
		return {
			**ci,
			'message': ci['error'],
			'B2skips': len(self.B2skips),
			'imageDownloadErrors': self.imageDownloadSkips,
		}


	def skips(self) :
		return self.totalSkipped() + len(self.B2skips) + len(self.imageDownloadSkips)


	def checkSkips(self) :
		self.checkingSkips = True

		maxlen = len(self.skipped) - 1
		for i in range(maxlen, -1, -1) :
			while self.skipped[i] :
				url = self.skipped[i].pop()
				if self.crawl(url) :
					pass  # use pass rather than not because it's easier to read
				elif i < maxlen :
					self.skipped[i+1].append(url)

		B2errors = len(self.B2skips)
		for _ in range(B2errors) :
			self.uploadToB2(*self.B2skips.pop(0))

		imageDownloadErrors = len(self.imageDownloadSkips)
		for _ in range(imageDownloadErrors) :
			self.crawl(self.imageDownloadSkips.pop(0))

		self.checkingSkips = False

		self.logger.info({
			'name': self.name,
			'remainingB2errors': len(self.B2skips),
			'B2errors': B2errors,
			'imageDownloadErrors': imageDownloadErrors,
			'remainingImageDownloadErrors': len(self.imageDownloadSkips),
		})


	def verboseSkipped(self) :
		return f'{self.skipped} {self.imageDownloadSkips}, B2 skips left: {len(self.B2skips)}'


	def postProcessing(self, item) :
		sha1, filename, extension = self.downloadimagefordatabase(item['thumbnails'][self.thumbnailIndex])
		item.update({
			'sha1': sha1,
			'filename': filename,
			'extension': extension,
			'tags': list(set(filter(None, map(normalizeTag, item['tags'])))),
			'rating': ratingmap[item['rating'].lower()],
		})


	def imageDownloadErrorHandler(self) :
		e = sys.exc_info()[1]
		self.logger.warning(f'{self.name} encountered {e.status} {GetFullyQualifiedClassName(e)}: {e} on id {self.id}.')
		self.imageDownloadSkips.append(self.url)


	def downloadimagefordatabase(self, imageurl) :
		response = None
		for _ in range(self.imageMaxRetries) :
			with requests.get(imageurl, stream=True, timeout=self.imageTimeout) as response :  # stream=True IS REQUIRED
				try :
					if response.ok :
						imagedata = response.content
						sha1 = hashlib_sha1(imagedata).hexdigest()
						validateSha1(sha1)
						parser = ImageFile.Parser()
						chunksize = 100
						for chunk in range(0, len(imagedata), chunksize) :
							parser.feed(imagedata[chunk:chunk+chunksize])
							if parser.image : break
						extension, mime = mimeTypeMap[parser.image.format]
						del parser
						filename = f'images/{sha1}.{extension}'
						with open(filename, 'wb') as image :  # can do this on another process
							image.write(imagedata)
						B2return = self.uploadToB2(imagedata, sha1, extension, mime)
						return sha1, filename, extension

				except Exception as e :
					if 'sha1' in str(e) :
						raise InvalidResponseType('website returned invalid image.', logdata={ 'sha1': sha1, **self.crashInfo() })
					# else pass, just retry

		status = response.status_code if response else -1
		raise ImageDownloadError(f'failed to download image for database. imageurl: {imageurl}', status=status)


	def uploadToB2(self, filedata, sha1, extension, contenttype) :
		# obtain upload url
		response = None
		upload = False
		for _ in range(self.B2MaxRetries) :
			try :
				response = requests.post(
					self.B2['apiUrl'] + '/b2api/v2/b2_get_upload_url',
					data='{ "bucketId": "' + self.B2['allowed']['bucketId'] + '" }',
					headers={ 'Authorization': self.B2['authorizationToken'] },
					timeout=self.timeout
				)
			except : pass
			else :
				if response.ok :
					upload = True
					break

				elif response.status_code == 401 :
					# obtain new auth token
					if not self._authorizeB2() :
						time.sleep(self.idleTime)

		if upload :
			# response ok
			response = json.loads(response.content)
			headers = { 'Authorization': response['authorizationToken'], 'X-Bz-File-Name': f'{sha1}.{extension}', 'Content-Type': contenttype, 'Content-Length': str(len(filedata)), 'X-Bz-Content-Sha1': sha1 }
			request = response['uploadUrl']
			for _ in range(self.B2MaxRetries) :
				try : response = requests.post(request, headers=headers, data=filedata, timeout=self.imageTimeout)
				except : pass
				else :
					if response.ok : return json.loads(response.content)

			if isinstance(response, requests.Response) :
				details = { 'error': 'image upload to B2 failed.', 'response': json.loads(response.content), 'status': response.status_code }
			else :
				details = { 'error': 'image upload to B2 failed.', 'response': response, 'status': None }
		elif response :
			# response not ok
			self.logger.error({
				'error': 'B2 image upload url handshake failed.',
				'name': self.name,
				'id': self.id,
				'response': json.loads(response.content),
				'status': response.status_code
			})
			raise ShutdownCrawler('B2 image upload url handshake failed.')
		else :
			# no response
			details = { 'error': 'B2 image upload url handshake failed.', 'response': str(response) }

		self.logger.info({
			'name': self.name,
			'id': self.id,
			**details,
		})

		# add to B2 skips as a tuple to expand again later
		self.B2skips.append((filedata, sha1, extension, contenttype))

		if len(self.B2skips) > self.B2cutoff :
			raise ShutdownCrawler(f'B2skips exceeded max length allowed ({self.B2cutoff}).')

		# sleep longer based on how many skipped images there are in memory currently
		time.sleep((len(self.B2skips) - 1)**2)


	def unblockCF(self, response) :
		GetCFCookies = getattr(import_module('cf.UnblockCF'), 'GetCFCookies')
		cookies = GetCFCookies(response)

		if 'cf_clearance' in cookies :
			self._session.cookies.update(cookies)
			self.logger.info({
				'name': self.name,
				'info': 'obtained new credentials.',
				'cookies': dict(cookies),
			})
		else :
			self.logger.error({
				'name': self.name,
				'info': 'failed to obtain new credentials.',
				'cookies': dict(cookies),
			})
