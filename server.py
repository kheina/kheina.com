from starlette.responses import FileResponse, PlainTextResponse
from pycrawl.common import GetFullyQualifiedClassName
from common.caching import SimpleCache
from HtmlTemplate import HtmlTemplate
from common.safejoin import SafeJoin
from collections import defaultdict
from urllib.parse import urlparse
from traceback import format_tb
from common.cwd import SetCWD
from common import HTTPError
try : import ujson as json
except : import json
import badger
import logger
import time
import sys
import os


SetCWD()
staticDirectory = 'static'
TEMPLATE = HtmlTemplate()


def getTheme(cookies) :
	return ' '.join([cookie for cookie in (cookies.get('theme'), cookies.get('accent')) if cookie])


@SimpleCache(900)  # 15 minute cache
def DefaultHeaders() :
	with open('serverheaders.json') as js :
		js = json.load(js)
		for key, value in js.items() :
			js[key] = '; '.join(value)
		return js


@SimpleCache(900)  # 15 minute cache
def JSOnLoad() :
	with open('JSOnLoad.json') as js :
		js = json.load(js)
		default = js.pop('default')
		return defaultdict(lambda : default, js)


async def jsLoader(req) :
	# no cache
	return PlainTextResponse(
		'; '.join(JSOnLoad()[urlparse(req.headers.get('referer')).path]),
		headers={
			'content-type': 'application/javascript',
			'cache-control': 'no-cache',
			**DefaultHeaders(),
		}
	)


# account
async def accountCreatePost(req) :
	formdata = await req.body()
	formdata = parsesource.parseformdata(formdata)
	if formdata['password'] != formdata['passwordRepeat'] :
		raise HTTPError.BadRequest('Passwords do not match.')
	if len(formdata['password']) < 10 :
		raise HTTPError.BadRequest('Password does not meet the requirements.')
	authServer.addUser(formdata['email'], formdata['username'], formdata['password'])
	return await TEMPLATE.format(
		SafeJoin(staticDirectory, 'error.html'),
		status_code=302,
		headers={ 'Location': '/account', **DefaultHeaders() },
		theme=getTheme(req.cookies),
	)


async def accountLoginPost(req) :
	formdata = await req.body()
	formdata = parsesource.parseformdata(formdata)
	key = authServer.verifyLogin(formdata['email'], formdata['password'])
	headers = None
	if key :
		headers = {
			'location': '/account',
			'username': key[1],
			'login-key': key[0]._login,
			'refresh-key': key[0]._refresh,
			**DefaultHeaders(),
		}
		status_code = 302
		error = 'your browser was not redirected'
	else :
		error = 'invalid email or password'
	return await TEMPLATE.format(
		SafeJoin(staticDirectory, 'error.html'),
		error=error,
		status_code=status_code,
		headers=DefaultHeaders(),
		theme=getTheme(req.cookies),
	)


pageHandlerSwitch = {
	'account/createPOST': accountCreatePost,
	'account/loginPOST': accountLoginPost,
}


async def handle_exception(e, req) :
	exc_type, exc_obj, exc_tb = sys.exc_info()
	stacktrace = format_tb(exc_tb)
	exc_obj = str(e)
	error = f'{GetFullyQualifiedClassName(e)}: {exc_obj}\n	stacktrace:\n'
	for framesummary in stacktrace :
		error = error + framesummary
	logdata = getattr(e, 'logdata', { })
	logger.agent.log_struct({
		'error': exc_obj,
		'stacktrace': stacktrace,
		'url': str(req.url),
		'method': req.method,
		**logdata,
	}, severity='WARNING' if type(e) is HTTPError.NotFound else 'ERROR')
	return error


async def HTMLErrorHandler(req, e) :
	error = await handle_exception(e, req)
	if issubclass(type(e), HTTPError.HTTPError) :
		status_code = e.status
		error = str(e.status) + ' ' + error
	else :
		status_code = 500
	return await TEMPLATE.format(
		SafeJoin(staticDirectory, 'error.html'),
		error=error,
		status_code=status_code,
		headers=DefaultHeaders(),
		theme=getTheme(req.cookies),
	)


# this should be the final route!
async def slash(req) :
	location = req.path_params['location']
	method = location + req.method
	try :
		safeLocation = SafeJoin(staticDirectory, location)
		if method in pageHandlerSwitch :
			await pageHandlerSwitch[method](req)
		elif os.path.isfile(safeLocation) :			
			return FileResponse(safeLocation)
		else :
			return await TEMPLATE.format(
				safeLocation + '/index.html',
				headers=DefaultHeaders(),
				theme=getTheme(req.cookies),
			)
	except Exception as e :
		return await HTMLErrorHandler(req, e)



async def startup():
	logger.start('server')
	logger.agent.log_text('server started', severity='INFO')

from starlette.applications import Starlette
from starlette.staticfiles import StaticFiles
from starlette.middleware import Middleware
from starlette.middleware.trustedhost import TrustedHostMiddleware
from starlette.routing import Route, Mount

allowed_hosts = { '127.0.0.1', 'kheina.com', '*.kheina.com' }

routes = [
	Mount('/static', app=StaticFiles(directory='static'), name='static'),
	Route('/onload.js', endpoint=jsLoader),
	Route('/{location:path}', endpoint=slash, methods=('GET', 'POST')),
]

app = Starlette(
	routes=routes,
	middleware=[Middleware(TrustedHostMiddleware, allowed_hosts=allowed_hosts)],
	on_startup=[startup],
)

if __name__ == '__main__' :
	from uvicorn.main import run
	run(app, host='127.0.0.1', port=80)

badger.createbadge('server-badge-template.svg', f'{staticDirectory}/badges/server.svg')
