from PIL import ImageFile
from traceback import format_tb
from base64 import b64encode
from common import HTTPError
try : import ujson as json
except : import json
import requests
import hashlib
import time
import sys

def isint(s) :
	try : return int(s)
	except : return None

def isfloat(s) :
	try : return float(s)
	except : return None

def isstr(s) :
	try: 
		str(s)
		return True
	except ValueError:
		return False

def contains(list, filter) :
	for i in range(len(list)-1, -1, -1) :
		if filter(list[i]) : return i
	return None	

class ImageError(Exception) :
	pass

acceptedMimeTypes = {
	'image/jpeg',
	'image/png',
	'image/gif',
	'application/octet-stream',
}

def loadimagedata(url) :
	with requests.get(url, stream=True) as response :  # stream=True IS REQUIRED
		if response.ok :
			if 'Content-Type' in response.headers and response.headers['Content-Type'] not in acceptedMimeTypes :
					raise HTTPError.UnsupportedMedia('File is not of type jpg, png, or gif.')
			if 'Content-Length' in response.headers and isint(response.headers['Content-Length']) > 8388608 :  # 8192kB in bytes
					raise ImageError('Image size greater than 8192kB.')
			image = ImageFile.Parser()
			for chunk in response.iter_content(chunk_size=1024) :
				image.feed(chunk)
				if image.image :
					x, y = image.image.size
					pixels = x*y
					if pixels > 56250000 :  # 56250000 = 7500*7500
						raise ImageError('Image contains more than 56250000 pixels (contains ' + str(pixels) + ')')
					return image.data + response.content
			raise HTTPError.UnsupportedMedia('File is not of type jpg, png, or gif.')

def processImageForSearch(imagedatastream) :
	pass

def getimageinfofromurl(url) :
	try :
		with requests.get(url, stream=True) as response :  # stream=True IS REQUIRED
			if response.status_code == 200 :
				image = ImageFile.Parser()
				for chunk in response.iter_content(chunk_size=512) :
					image.feed(chunk)
					if image.image :
						x, y = image.image.size
						pixels = x*y
						return (x,y,pixels)
	except : return None

def parseformdata(form) :
	start = 0
	end = 0
	data = { }
	while start >= 0 and end >= 0 :
		tempdict = { }
		name = ''
		#start = form.find(b'Content-Disposition:', end)  # 20
		#if start >= 0 :
		#	end = form.find(b';', start+20)
		#	if end > 0 :
		#		contentdisposition = form[start+21:end]
		#		print (contentdisposition)
		start = form.find(b'name=', end)  # 5
		if start >= 0 :
			start = form.find(b'"', start+5)
			if start >= 0 :
				start = start+1
				end = form.find(b'"', start)
				name = form[start:end].decode()
		start = form.find(b'filename=', end)  # 9
		if start >= 0 :
			start = form.find(b'"', start+5)
			if start >= 0 :
				start = start+1
				end = form.find(b'"', start)
				if end > start : tempdict['filename'] = form[start:end].decode()
		start = form.find(b'Content-Type:', end)  # 13
		if start >= 0 :
			start = form.find(b'\r\n\r\n', end) + 4
			end = form.find(b'-----', start)
			if end > start+2 :
				tempdict['contenttype'] = form[start:end]
				start = end
				end = start - 1
			else : name = ''
		if end > start :  # scoop up the rest
			start = end+1
			end = form.find(b'-----', start)
			if end > start+1 :
				misc = form[start:end].decode().strip()
				tempdict['misc'] = misc
		if name != '' :
			length = len(tempdict)
			if length == 1 and len(misc) > 0:
				data[name] = misc
			elif length > 1 : 
				data[name] = tempdict
	return data

def start() :
	global B2

	# initialize B2
	B2 = ''

	# credentials = {
	# 	'furaffinity' : {
	# 		'a' : 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
	# 		'b' : 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
	# 	}
	#   'weasyl': { 
	#		'WZL' : 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
	# 	}
	# }
	# credentials are saved in credentials in the format above

	print('loading credentials...', end='', flush= True)
	with open('credentials/crawlers.json', 'r') as credentials :
		credentials = json.load(credentials)
		B2 = credentials['B2']
		#print(json.dumps(credentials, indent=2))
	print('success.')
	
	basic_auth_string = b'Basic ' + b64encode((B2['keyId'] + ':' + B2['key']).encode())
	headers = {'Authorization': basic_auth_string}

	response = requests.get('https://api.backblazeb2.com/b2api/v2/b2_authorize_account', headers=headers)
	if response.status_code == 200 :
		B2 = json.loads(response.content)
	else : print(response.content)
	iqdbsize = -1
	with open('iqdbsize.txt', 'r') as iqdbsizefile :
		iqdbsize = int(iqdbsizefile.read())
	print('iqdbsize:', iqdbsize)

def uploadToB2(filedata, filename, sha1, contenttype) :
	global B2
	headers = {'Authorization': B2['authorizationToken']}  #'X-Bz-File-Name': filename, 'Content-Type': contenttype, 'Content-Length': sys.getsizeof(filedata), 'X-Bz-Content-Sha1': sha1}\
	data = {'bucketId' : B2['allowed']['bucketId']}
	response = requests.post(B2['apiUrl'] + '/b2api/v2/b2_get_upload_url', data='{"bucketId":"' + B2['allowed']['bucketId'] + '"}', headers=headers)
	try :
		if response.status_code == 200 :
			response = json.loads(response.content)
			headers = {'Authorization': response['authorizationToken'], 'X-Bz-File-Name': filename, 'Content-Type': contenttype, 'Content-Length': str(sys.getsizeof(filedata)), 'X-Bz-Content-Sha1': sha1}
			request = response['uploadUrl']
			attempts = 5
			for i in range(attempts) : 
				#print('\r',i, end=' ', flush=True)
				response = requests.post(request, headers=headers, data=filedata)
				if response.status_code == 200 : return json.loads(response.content)
				#elif response.status_code == 401 : 
			print('failed to upload', filename, 'to B2')
		else : print(response.content)
	except Exception as e :
		exc_type, exc_obj, exc_tb = sys.exc_info()
		stacktrace = format_tb(exc_tb)
		error = 'ERROR: ' + str(e) + ', stacktrace:\n'
		for framesummary in stacktrace :
			error = error + framesummary
		print(error)
	return None

def uploadurltoB2(url) :
	imagedata = loadimagedata(url)
	hashslingingslasher = hashlib.sha1()
	hashslingingslasher.update(imagedata)
	sha1 = hashslingingslasher.hexdigest()
	filename = sha1 + '.jpg'
	return uploadToB2(imagedata, filename, sha1, 'image/jpeg')

def uploadfiletoB2(filename) :
	imagedata = b''
	with open(filename, 'rb') as image :
		imagedata = image.read()
	hashslingingslasher = hashlib.sha1()
	hashslingingslasher.update(imagedata)
	sha1 = hashslingingslasher.hexdigest()
	return uploadToB2(imagedata, sha1 + '.jpg', sha1, 'image/jpeg')
