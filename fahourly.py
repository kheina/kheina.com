import parsesource
import sched, time


def update() :
	currenttime = time.time()
	url = 'https://www.furaffinity.net/aup'
	html = parsesource.FAgethtml(url)
	fausers = parsesource.FAgetusers(html)
	print(readabletime(time.time()), 'registered:', fausers['registered'], 'other:', fausers['other'], 'guests:', fausers['guests'])
	schedule_nextupdate()

def schedule_nextupdate() :
	global scheduler
	global timer  # seconds
	nextupdate = time.time()
	nextupdate = (nextupdate - (nextupdate % timer)) + timer
	scheduler.enterabs(nextupdate, 1, update, ())

def readabletime(currenttime) :
	readable = ''
	localtime = time.localtime(currenttime)
	if localtime.tm_hour < 10 : readable = readable + '0'
	readable = readable + str(localtime.tm_hour) + ':'
	if localtime.tm_min  < 10 : readable = readable + '0'
	readable = readable + str(localtime.tm_min)
	return readable

def printtime() :
	global scheduler
	global timer  # seconds
	nextupdate = time.time()
	nextupdate = (nextupdate - (nextupdate % timer)) + timer
	print (readabletime(time.time()), readabletime(nextupdate))


if __name__ == '__main__' :
	global scheduler
	global timer  # seconds
	timer = 3600
	parsesource.start()
	scheduler = sched.scheduler(time.time, time.sleep)
	schedule_nextupdate()
	scheduler.run()