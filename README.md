### For in-depth details on how this project works, please refer to the [documentation](https://gitlab.com/kheina/kheina.com/wikis/home)  

#### To contribute to this project, check out the [todo list](https://gitlab.com/kheina/kheina.com/blob/master/TODO.md) as well as the [issue tracker](https://gitlab.com/kheina/kheina.com/issues)

[Opensuse](https://www.opensuse.org) is recommended for this project due to the difficulty of finding certain packages on other versions of linux.  
These opensuse packages are required to run this project:  
`gcc-c++`  
`gd-devel`  
`libX11-devel`  
`fontconfig-devel`  
`libpng12-devel`  
`python3-devel`  
  
  
These python packages are required to run this project:  
`colorama`  
`psycopg2-binary` (or `psycopg2`)  
`psycopg2.extras`  
`pillow`  
`bs4`  
`bocadillo`  
[`ujson` not required, but highly recommended]  
  
  
Some frequently used terminal commands:  
`./iqdb/iqdb listen 7000 -r -s127.0.0.1 main.db &`  
`nohup python3 crawlers.py &` and `truncate nohup.out --size=0`  
`sudo gunicorn -w 1 -k uvicorn.workers.UvicornWorker -b 0.0.0.0:80 server:api &`  
`sudo ssh -i "desktopssh.pem" ec2-user@[DNS] -o StrictHostKeyChecking=no`  